# TTK4145 Elevator Project

The project is based around the students creating a system that controls N elevators. This is done by using concurrency and transactions while having a large focus on things like fault tolerance and reliability.

The main task is to make sure that no elevator lamp is lit unless the order that it represents is fullfilled. This means that no orders should be lost, what so ever!

The code is organized according to https://github.com/golang-standards/project-layout

There are some UML diagrams in the `docs` folder, but these are from the early design review and not up to date.

## Running the elevators

The elevators is created to be run in a [docker container](https://www.docker.com/). This means that the docker daemon will be responsible for restarting stopped or crashed elevators. To make it easy to start multiple containers and configure them [docker-compose](https://docs.docker.com/compose/) is also used.

To build and run the containers run the following commands

```sh
make docker-run # This runs two elevators logging to stdout
```

as of now the docker-compose file is created to work with two elevator simulators. So to start the system for testing you can run the commands above and then run two simulators. One on port `9000` and one on port `9001`. There is a script that runs two simulators in tmux in the `scripts` folder.

To run without docker use the added Makefile.
Build and run with

```sh
make run
```

To change parameters edit them in the Makefile.

## Parameters

There are a couple of configurable parameters

- `NUM_FLOOR` which indicates how many floors there are in each elevator.
- `ELEVATOR_ADDRESS` on the form `{ip/hostname}:{port}` indicates where the simulator or driver is reachable.
- `ELEVATOR_ID` a four character string parameter that is used to set the elevators unique ID. All values, except all zero bytes, are accepted.

To set these export them to the environment prior to running the elevator program.

## Testing

To test the system a few functions has been created for easy testing purposes.
To enable them run

```sh
source scripts/elevator_testing_commands.sh
```

This enables the commands `elev_enable_packet_drop`,`elev_disconnect_elevators` and `elev_reenable_ports`.

The two first ones affects ports 9000-9004, and all commands needs root access. The last command flushes the iptable rules.

## Design descisions

### UDP over TCP

We ended up using UDP sockets to distribute our orders because of high throughtput and ease of development.

### How many packages guarantees that some other node receives it?

We are set in a situation where we have a 20% packet loss. This means that we needs to send a lot more
packets that we would need if we used TCP. So if we send `N` and we assume that all packet losses are
independent, which is probably not the best assumption because network traffic often happens in
surges, so we will up our requirement for the probability of losing a packet.
So under these assumptions we can see that for different `N` we get these probabilites:

| N   | Probability of losing an order | Probability of losing 1 of 10000 orders |
| --- | ------------------------------ | --------------------------------------- |
| 100 | 1.2676e-70                     | 1.2676e-66                              |
| 90  | 1.2379e-63                     | 1.2379e-59                              |
| 80  | 1.2089e-56                     | 1.2089e-52                              |
| 70  | 1.1805e-49                     | 1.1805e-45                              |
| 60  | 1.1529e-42                     | 1.1529e-38                              |
| 50  | 1.1258e-35                     | 1.1258e-31                              |
| 40  | 1.0995e-28                     | 1.0995e-24                              |
| 30  | 1.0737e-21                     | 1.0737e-17                              |
| 20  | 1.0485e-14                     | 1.0485e-10                              |
| 10  | 1.0240e-07                     | 0.0010                                  |

This is based on a binomial distribution.
Based on these data we have chosen 40 orders is acceptable for this spec.

### Order backups

One solution of order backups is to save the orders to disk, this is both slow and it is susceptible to harddrive corruption. So our solution is to save all orders distributed in the network. By sending out all received orders, as well as cab orders into the network, if an elevator crashes a deadline for completing orders will trigger in some of the other elevators and that elevator will rebroadcast the timed out order, this will then be accepted by the crashed elevator and it will be able to serve the orders once again.
