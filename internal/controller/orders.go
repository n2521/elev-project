package controller

import (
	ordermgr "github.com/Oskiboy/elev-project/internal/orders"
	"github.com/TTK4145/driver-go/elevio"
)

// AtFloor checks if there is an order at the given floor
func orderAtFloor(orders *[]ordermgr.Order, floor int, dir elevio.MotorDirection) bool {

	var orderAbove, orderBelow, upOrder, downOrder bool = false, false, false, false

	// Check if there any orders at our floor or any above/below us.
	// Here we could use the orderInDirection function, but we already have to
	// loop over the orders list, so might as well do it here for a slight
	// performance increase
	for _, order := range *orders {
		if order.Floor == floor {
			switch order.ButtonType {
			case elevio.BT_HallUp:
				upOrder = true
			case elevio.BT_HallDown:
				downOrder = true
			case elevio.BT_Cab:
				return true // We always serve cab orders.
			}
		} else if order.Floor < floor {
			orderBelow = true
		} else if order.Floor > floor {
			orderAbove = true
		}
	}

	// We only want to serve orders that is in the same direction as
	// we are currenlty moving, unless there is no more orders in our direction
	switch dir {
	case elevio.MD_Up:
		return upOrder || !orderAbove && downOrder
	case elevio.MD_Down:
		return downOrder || !orderBelow && upOrder
	case elevio.MD_Stop:
		return upOrder || downOrder
	}
	return false
}

// orderInDirection checks if there is an order in the given direction
func orderInDirection(orders *[]ordermgr.Order, floor int, dir elevio.MotorDirection) bool {
	if dir == elevio.MD_Stop {
		return orderAtFloor(orders, floor, dir)
	}
	for _, o := range *orders {
		if o.Floor < floor && dir == elevio.MD_Down {
			return true
		} else if o.Floor > floor && dir == elevio.MD_Up {
			return true
		}
	}
	return false
}

// clearOrdersAtFloor clears all orders from a given floor
func clearOrdersAtFloor(orders *[]ordermgr.Order, floor int, cOrders chan<- ordermgr.Order) {
	ordersToBeCleared := []ordermgr.Order{}
	for _, o := range *orders {
		if o.Floor == floor {
			ordersToBeCleared = append(ordersToBeCleared, o)
		}
	}
	for _, o := range ordersToBeCleared {
		cOrders <- o
		removeOrder(orders, o)
	}
}

// clearExternalOrders handles clearing orders that have been completed in other elevators.
func clearExternalOrders(orders *[]ordermgr.Order, order ordermgr.Order) {
	ordersToBeCleared := []ordermgr.Order{}
	for _, o := range *orders {
		if o.Floor == order.Floor && o.ButtonType != elevio.BT_Cab {
			ordersToBeCleared = append(ordersToBeCleared, o)
		}
	}
	for _, o := range ordersToBeCleared {
		removeOrder(orders, o)
	}
}

// removeOrder removes a given order from the given list.
func removeOrder(orders *[]ordermgr.Order, order ordermgr.Order) {
	if len(*orders) == 0 {
		return
	}
	if len(*orders) == 1 {
		if (*orders)[0] == order {
			*orders = []ordermgr.Order{}
		} else {
			return
		}
	} else {
		for i := range *orders {
			if (*orders)[i] == order {
				copy((*orders)[i:], (*orders)[i+1:])
				break
			}
		}
		*orders = (*orders)[:len(*orders)-1]
	}
}

// Finds out if there are any orders at all and which direction they are in
func anyOrder(orders *[]ordermgr.Order, floor int) (elevio.MotorDirection, bool) {
	if len(*orders) == 0 {
		return elevio.MD_Stop, false
	}
	var dir elevio.MotorDirection = elevio.MD_Down
	if (*orders)[0].Floor < floor {
		dir = elevio.MD_Down
	} else if (*orders)[0].Floor > floor {
		dir = elevio.MD_Up
	}

	return dir, true
}

func addOrder(orders *[]ordermgr.Order, order ordermgr.Order) {
	for _, o := range *orders {
		if o.ButtonType == order.ButtonType &&
			o.Floor == order.Floor {
			return
		}
	}
	*orders = append(*orders, order)
}
