package controller

import (
	"log"
	"time"

	"github.com/Oskiboy/elev-project/internal/elevator"
	ordermgr "github.com/Oskiboy/elev-project/internal/orders"
	"github.com/TTK4145/driver-go/elevio"
)

var (
	orders []ordermgr.Order
)

const (
	stateMachineRate = time.Second / 50
)

// Run starts the elevator controller, it spawns a goroutine and then returns
func Run(newOrders, clearedOrders <-chan ordermgr.Order, clearOrders chan<- ordermgr.Order) {
	log.Println("Starting elevator FSM")
	go stateMachine(newOrders, clearedOrders, clearOrders)
}

func stateMachine(newOrders, clearedOrders <-chan ordermgr.Order, clearOrders chan<- ordermgr.Order) {
	orders := new([]ordermgr.Order)

	var timeout time.Time = time.Now()
	rate := time.Tick(stateMachineRate)

	elevator.GoInDirection(elevio.MD_Up)

	state := elevator.GetState()
	log.Println("Got initial state, running FSM")
	var lastDirection = state.Direction
	for {
		<-rate // Cap the max rate of the state machine.
		select {
		case o := <-newOrders:
			addOrder(orders, o)
		case o := <-clearedOrders:
			clearExternalOrders(orders, o)
		default:
		}
		state = elevator.GetState()
		orderAtCurrentFloor := orderAtFloor(orders, state.Floor, state.Direction)

		switch state.Direction {
		case elevio.MD_Up:
			if state.Floor == 3 || orderAtCurrentFloor {
				elevator.StopMotor()
				if orderAtCurrentFloor {
					timeout = time.Now().Add(3 * time.Second)
					clearOrdersAtFloor(orders, state.Floor, clearOrders)
				}
			}

		case elevio.MD_Down:
			if state.Floor == 0 || orderAtCurrentFloor {
				elevator.StopMotor()
				if orderAtCurrentFloor {
					timeout = time.Now().Add(3 * time.Second)
					clearOrdersAtFloor(orders, state.Floor, clearOrders)
				}
			}
		case elevio.MD_Stop:
			if timeout.Before(time.Now()) {
				dir, any := anyOrder(orders, state.Floor)
				if orderInDirection(orders, state.Floor, lastDirection) {
					elevator.GoInDirection(lastDirection)
				} else if any {
					elevator.GoInDirection(dir)
					lastDirection = dir
					elevator.Serving(dir)
				} else if state.ServingOrder != elevio.MD_Stop {
					elevator.NotServing()
				}
			}

			if orderAtCurrentFloor {
				clearOrdersAtFloor(orders, state.Floor, clearOrders)
				timeout = time.Now().Add(3 * time.Second)
			}
		}

	}
}
