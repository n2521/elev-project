package controller

import (
	"sync"
	"testing"
	"time"

	ordermgr "github.com/Oskiboy/elev-project/internal/orders"
	"github.com/TTK4145/driver-go/elevio"
)

type testCase struct {
	caseName         string
	orders           *[]ordermgr.Order
	floor            int
	dir              elevio.MotorDirection
	ordersAbove      bool
	ordersBelow      bool
	ordersAtFloor    bool
	orderInDirection bool
	anyOrder         bool
	anyOrderDir      elevio.MotorDirection
}

var tc = []testCase{
	testCase{
		caseName:         "No orders",
		orders:           &[]ordermgr.Order{},
		floor:            2,
		dir:              elevio.MD_Stop,
		ordersAbove:      false,
		ordersBelow:      false,
		ordersAtFloor:    false,
		orderInDirection: false,
		anyOrder:         false,
	},
	testCase{
		caseName: "One up order at floor 1 from standstill at floor 2",
		orders: &[]ordermgr.Order{
			ordermgr.Order{
				Floor:      1,
				ButtonType: elevio.BT_HallUp,
			},
		},
		floor:            2,
		dir:              elevio.MD_Stop,
		ordersAbove:      false,
		ordersBelow:      true,
		ordersAtFloor:    false,
		orderInDirection: false,
		anyOrder:         true,
		anyOrderDir:      elevio.MD_Down,
	},
	testCase{
		caseName: "One up order at floor 2 going up at floor 2",
		orders: &[]ordermgr.Order{
			ordermgr.Order{
				Floor:      2,
				ButtonType: elevio.BT_HallUp,
			},
		},
		floor:            2,
		dir:              elevio.MD_Up,
		ordersAbove:      false,
		ordersBelow:      false,
		ordersAtFloor:    true,
		orderInDirection: false,
		anyOrder:         true,
		anyOrderDir:      elevio.MD_Up,
	},
	testCase{
		caseName: "One up order at floor 3 going up from floor 2",
		orders: &[]ordermgr.Order{
			ordermgr.Order{
				Floor:      3,
				ButtonType: elevio.BT_HallUp,
			},
		},
		floor:            2,
		dir:              elevio.MD_Up,
		ordersAbove:      true,
		ordersBelow:      false,
		ordersAtFloor:    false,
		orderInDirection: true,
		anyOrder:         true,
		anyOrderDir:      elevio.MD_Up,
	},
}

func Test_orderAtFloor(t *testing.T) {
	for _, tt := range tc {
		t.Run(tt.caseName, func(t *testing.T) {
			if got := orderAtFloor(tt.orders, tt.floor, tt.dir); got != tt.ordersAtFloor {
				t.Errorf("orderAtFloor() = %v, want %v", got, tt.ordersAtFloor)
			}
		})
	}
}

func Test_orderInDirection(t *testing.T) {
	for _, tt := range tc {
		t.Run(tt.caseName, func(t *testing.T) {
			if got := orderInDirection(tt.orders, tt.floor, tt.dir); got != tt.orderInDirection {
				t.Errorf("orderInDirection() = %v, want %v", got, tt.orderInDirection)
			}
		})
	}
}

func Test_anyOrder(t *testing.T) {
	for _, tt := range tc {
		t.Run(tt.caseName, func(t *testing.T) {
			if gotD, got := anyOrder(tt.orders, tt.floor); got != tt.anyOrder && gotD != tt.anyOrderDir {
				t.Errorf("anyOrder() = %v, want %v", got, tt.orderInDirection)
			}
		})
	}
}

func Test_addOrder(t *testing.T) {
	type args struct {
		orders *[]ordermgr.Order
		order  ordermgr.Order
	}
	tests := []struct {
		name string
		args args
		want *[]ordermgr.Order
	}{
		{
			name: "Adding single order to order list",
			args: args{
				orders: &[]ordermgr.Order{},
				order: ordermgr.Order{
					Floor:      1,
					ButtonType: elevio.BT_Cab,
				},
			},
			want: &[]ordermgr.Order{
				ordermgr.Order{
					Floor:      1,
					ButtonType: elevio.BT_Cab,
				},
			},
		},
		{
			name: "Adding duplicate order should not add another order",
			args: args{
				orders: &[]ordermgr.Order{
					ordermgr.Order{
						Floor:      1,
						ButtonType: elevio.BT_Cab,
					},
				},
				order: ordermgr.Order{
					Floor:      1,
					ButtonType: elevio.BT_Cab,
				},
			},
			want: &[]ordermgr.Order{
				ordermgr.Order{
					Floor:      1,
					ButtonType: elevio.BT_Cab,
				},
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			addOrder(tt.args.orders, tt.args.order)
			if len(*tt.args.orders) != len(*tt.want) {
				t.Errorf("removeOrder() failed got %v, wanted %v", tt.args.orders, tt.want)
			}
			for i := range *tt.args.orders {
				if (*tt.args.orders)[i] != (*tt.want)[i] {
					t.Errorf("removeOrder() failed got %v, wanted %v", tt.args.orders, tt.want)
				}
			}
		})
	}
}

func Test_removeOrder(t *testing.T) {
	type args struct {
		orders *[]ordermgr.Order
		order  ordermgr.Order
	}
	tests := []struct {
		name string
		args args
		want *[]ordermgr.Order
	}{
		{
			name: "Trying to remove a non existing order should not fail",
			args: args{
				orders: &[]ordermgr.Order{},
				order: ordermgr.Order{
					Floor:      1,
					ButtonType: elevio.BT_Cab,
				},
			},
			want: &[]ordermgr.Order{},
		},
		{
			name: "Trying to remove a non existing order should not remove non equal elements",
			args: args{
				orders: &[]ordermgr.Order{
					ordermgr.Order{
						Floor:      2,
						ButtonType: elevio.BT_Cab,
					},
				},
				order: ordermgr.Order{
					Floor:      1,
					ButtonType: elevio.BT_Cab,
				},
			},
			want: &[]ordermgr.Order{
				ordermgr.Order{
					Floor:      2,
					ButtonType: elevio.BT_Cab,
				},
			},
		},
		{
			name: "Removing a single order should empty a single order list",
			args: args{
				orders: &[]ordermgr.Order{
					ordermgr.Order{
						Floor:      1,
						ButtonType: elevio.BT_Cab,
					},
				},
				order: ordermgr.Order{
					Floor:      1,
					ButtonType: elevio.BT_Cab,
				},
			},
			want: &[]ordermgr.Order{},
		},
		{
			name: "Removing a single order should remove it from the order list",
			args: args{
				orders: &[]ordermgr.Order{
					ordermgr.Order{
						Floor:      1,
						ButtonType: elevio.BT_Cab,
					},
					ordermgr.Order{
						Floor:      2,
						ButtonType: elevio.BT_Cab,
					},
				},
				order: ordermgr.Order{
					Floor:      1,
					ButtonType: elevio.BT_Cab,
				},
			},
			want: &[]ordermgr.Order{
				ordermgr.Order{
					Floor:      2,
					ButtonType: elevio.BT_Cab,
				},
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			removeOrder(tt.args.orders, tt.args.order)
			if len(*tt.args.orders) != len(*tt.want) {
				t.Errorf("removeOrder() failed got %v, wanted %v", tt.args.orders, tt.want)
			}
			for i := range *tt.args.orders {
				if (*tt.args.orders)[i] != (*tt.want)[i] {
					t.Errorf("removeOrder() failed got %v, wanted %v", tt.args.orders, tt.want)
				}
			}
		})
	}
}

func Test_clearOrdersAtFloor(t *testing.T) {
	type args struct {
		orders  *[]ordermgr.Order
		floor   int
		cOrders chan ordermgr.Order
	}
	tests := []struct {
		name       string
		args       args
		want       *[]ordermgr.Order
		chanChecks int
	}{
		{
			name: "Trying to clear order from empty list should not fail",
			args: args{
				orders:  &[]ordermgr.Order{},
				floor:   1,
				cOrders: make(chan ordermgr.Order),
			},
			want:       &[]ordermgr.Order{},
			chanChecks: 0,
		},
		{
			name: "Clearing 3 orders from one floor",
			args: args{
				orders: &[]ordermgr.Order{
					{
						Floor:      1,
						ButtonType: elevio.BT_Cab,
					},
					{
						Floor:      1,
						ButtonType: elevio.BT_HallUp,
					},
					{
						Floor:      1,
						ButtonType: elevio.BT_HallDown,
					},
				},
				floor:   1,
				cOrders: make(chan ordermgr.Order),
			},
			want:       &[]ordermgr.Order{},
			chanChecks: 3,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			// To drain the channel
			var wg sync.WaitGroup
			wg.Add(1)
			go func(wg *sync.WaitGroup) {
				for i := 0; i < tt.chanChecks; i++ {
					<-tt.args.cOrders
				}
				wg.Done()
			}(&wg)
			clearOrdersAtFloor(tt.args.orders, tt.args.floor, tt.args.cOrders)
			timedOut := false
			c := make(chan struct{})
			go func() {
				defer close(c)
				wg.Wait()
			}()
			select {
			case <-c:
			case <-time.After(10 * time.Millisecond):
				timedOut = true
			}
			if timedOut {
				t.Error("waiting for clearOrdersAtFloor() failed, possible lock")
			}
		})
	}
}
