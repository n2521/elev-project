package elevator

import (
	"encoding/binary"
	"errors"

	"github.com/TTK4145/driver-go/elevio"
)

//StateBinarySize tells us how large the State is when marshalled to binary.
const StateBinarySize = 9

// State holds the current state of an
// elevator
// It is represented on binary form as
// Data		| ID		| Floor		| Flags	|
// Byte#	| 1 2 3 4	| 5 6 7 8	| 9		|
type State struct {
	ID           [4]byte
	Direction    elevio.MotorDirection
	Floor        int
	MotorRunning bool
	StopPressed  bool
	Obstruction  bool
	ServingOrder elevio.MotorDirection
}

// MarshalBinary creates a binary representation of an State
func (e *State) MarshalBinary() ([]byte, error) {
	var flags uint8 = 0

	if e.MotorRunning {
		flags |= 0x1 << 0
	}
	if e.StopPressed {
		flags |= 0x1 << 1
	}
	if e.Obstruction {
		flags |= 0x1 << 2
	}
	switch e.Direction {
	case elevio.MD_Up:
		flags |= 0x1 << 6
	case elevio.MD_Down:
		flags |= 0x2 << 6
	case elevio.MD_Stop:
		flags |= 0x0 << 6
	}

	switch e.ServingOrder {
	case elevio.MD_Up:
		flags |= 0x1 << 4
	case elevio.MD_Down:
		flags |= 0x2 << 4
	case elevio.MD_Stop:
		flags |= 0x0 << 4
	}

	b := make([]byte, StateBinarySize)
	copy(b[0:4], e.ID[:])
	binary.BigEndian.PutUint32(b[4:8], uint32(e.Floor))
	b[8] = flags
	return b, nil
}

// UnmarshalBinary populates an State from binary data.
func (e *State) UnmarshalBinary(data []byte) error {
	copy(e.ID[:], data[0:4])
	e.Floor = int(binary.BigEndian.Uint32(data[4:8]))
	flags := data[8]

	e.MotorRunning = flags&(0x1<<0) != 0
	e.StopPressed = flags&(0x1<<1) != 0
	e.Obstruction = flags&(0x1<<2) != 0
	switch flags >> 6 {
	case 0x0:
		e.Direction = elevio.MD_Stop
	case 0x1:
		e.Direction = elevio.MD_Up
	case 0x2:
		e.Direction = elevio.MD_Down
	default:
		return errors.New("could not unmarshal direction")
	}
	switch flags >> 4 & 0x3 {
	case 0x0:
		e.ServingOrder = elevio.MD_Stop
	case 0x1:
		e.ServingOrder = elevio.MD_Up
	case 0x2:
		e.ServingOrder = elevio.MD_Down
	default:
		return errors.New("could not unmarshal serving order")
	}
	return nil
}
