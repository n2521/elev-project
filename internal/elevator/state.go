package elevator

import (
	"log"
	"os"
	"strconv"
	"time"

	"github.com/TTK4145/driver-go/elevio"
)

// Request is an empty struct because we want to be able to extract
// the state from the elevator by passing some data into a channel and then
// having the elevator return the state. This is a mean of synchronization that
// removes the need of a mutex or semaphore which would reduce performance
// by a lot.
type Request struct{}

var (
	state         State
	directionChan chan elevio.MotorDirection
	servingChan   chan elevio.MotorDirection
	stateChan     chan State
	reqChan       chan Request
	initialized   bool
)

const (
	timeBetweenFloors = 3 * time.Second
)

func init() {
	initialized = false
	state = State{MotorRunning: true}
	directionChan = make(chan elevio.MotorDirection, 3)
	servingChan = make(chan elevio.MotorDirection, 3)
	stateChan = make(chan State, 100)
	reqChan = make(chan Request, 100)
}

// Initialize starts the elevator event loop after
// setting parameters from the environment.
// Available settings are:
// ELEVATOR_ADDRESS - Where the elevator driver runs(or simulator)
// NUM_FLOOR		- How many floors there are.
func Initialize(ID [4]byte) error {
	if initialized {
		return nil
	}
	var err error = nil
	address := os.Getenv("ELEVATOR_ADDRESS")
	if address == "" {
		address = "localhost:15657"
	}
	num := 4
	number, err := strconv.Atoi(os.Getenv("NUM_FLOOR"))
	if err != nil {
		log.Println("Could not parse NUM_FLOOR, got:", os.Getenv("NUM_FLOOR"), "using NUM_FLOOR=4")
	} else {
		num = number
	}

	state.ID = ID
	log.Println("Initializing elevator hardware")
	elevio.Init(address, num)

	for floor := 0; floor < num; floor++ {
		elevio.SetButtonLamp(elevio.BT_Cab, floor, false)
		if floor != 0 {
			elevio.SetButtonLamp(elevio.BT_HallDown, floor, false)
		}
		if floor != num-1 {
			elevio.SetButtonLamp(elevio.BT_HallUp, floor, false)
		}
	}

	log.Println("Running elevator on address:", address, "with", num, "elevators")
	go stateHandler()

	initialized = true
	return err
}

// stateHandler runs the elevator driver layer.
// It is responsible for checking inputs from the driver
// and updating the elevator state accordingly.
// It is also responsible for serving updated states to
// modules that request it see the GetState() function.
func stateHandler() {
	floorSensor := make(chan int)
	stopButton := make(chan bool)
	obstructionSwitch := make(chan bool)

	go elevio.PollFloorSensor(floorSensor)
	go elevio.PollStopButton(stopButton)
	go elevio.PollObstructionSwitch(obstructionSwitch)
	log.Println("Running state handler")

	motorStopDeadline := time.NewTimer(timeBetweenFloors)
	for {
		select {
		case <-motorStopDeadline.C:
			state.MotorRunning = false
			log.Println("Motor stop detected")
		case newDir := <-directionChan:
			if state.Direction == elevio.MD_Stop && newDir != elevio.MD_Stop {
				motorStopDeadline.Reset(timeBetweenFloors)
			} else if state.Direction != elevio.MD_Stop && newDir == elevio.MD_Stop {
				motorStopDeadline.Stop()
			}
			state.Direction = newDir
			elevio.SetMotorDirection(newDir)
		case newServ := <-servingChan:
			state.ServingOrder = newServ
		case floor := <-floorSensor:
			if state.Direction != elevio.MD_Stop {
				motorStopDeadline.Reset(timeBetweenFloors)
				if !state.MotorRunning {
					state.MotorRunning = true
					log.Println("Motor is functional again")
				}
			}
			state.Floor = floor
			elevio.SetFloorIndicator(floor)
		case stop := <-stopButton:
			state.StopPressed = stop
		case obs := <-obstructionSwitch:
			state.Obstruction = obs
		case <-reqChan:
			stateChan <- state
		}
	}
}

// StopMotor stops the motor and updates the state
// Will panic if Initialize is not run first!
func StopMotor() {
	if !initialized {
		log.Panicln("Elevator accessed before initialization!")
	}
	directionChan <- elevio.MD_Stop
}

// GoInDirection sets correct Direction and updates the state
// Will panic if Initialize is not run first!
func GoInDirection(dir elevio.MotorDirection) {
	if !initialized {
		log.Panicln("Elevator accessed before initialization!")
	}
	directionChan <- dir
}

// GetState returns the current state from the state handler.
// Will panic if Initialize is not run first!
func GetState() State {
	if !initialized {
		log.Panicln("Elevator accessed before initialization!")
	}
	reqChan <- Request{}
	s := <-stateChan
	return s
}

// NotServing sets the ServingOrder field to stop, which indicates that
// the elevator is idle.
func NotServing() {
	servingChan <- elevio.MD_Stop
}

// Serving seths the ServingOrder field to the given direction
// indicating that the elevator is currently serving in this direction
func Serving(dir elevio.MotorDirection) {
	servingChan <- dir
}
