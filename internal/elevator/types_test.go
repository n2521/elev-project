package elevator

import (
	"reflect"
	"testing"

	"github.com/TTK4145/driver-go/elevio"
)

func TestElevatorState_MarshalBinary(t *testing.T) {
	type fields struct {
		ID           [4]byte
		Direction    elevio.MotorDirection
		Floor        int
		MotorRunning bool
		StopPressed  bool
		Obstruction  bool
		ServingOrder elevio.MotorDirection
	}
	tests := []struct {
		name    string
		fields  fields
		want    []byte
		wantErr bool
	}{
		{
			name: "Going up from floor 3",
			fields: fields{
				ID:           [4]byte{1, 2, 3, 4},
				Direction:    elevio.MD_Up,
				Floor:        3,
				MotorRunning: true,
				StopPressed:  false,
				Obstruction:  false,
				ServingOrder: elevio.MD_Up,
			},
			want:    []byte{1, 2, 3, 4, 0, 0, 0, 3, 0x1<<6 | 0x1<<0 | 0x1<<4},
			wantErr: false,
		},
		{
			name: "Going down from floor 2 with stop pressed",
			fields: fields{
				ID:           [4]byte{0xDE, 0xAD, 0xBE, 0xEF},
				Direction:    elevio.MD_Down,
				Floor:        2,
				MotorRunning: true,
				StopPressed:  true,
				Obstruction:  false,
				ServingOrder: elevio.MD_Down,
			},
			want:    []byte{0xDE, 0xAD, 0xBE, 0xEF, 0, 0, 0, 2, 0x2<<6 | 0x1<<0 | 0x1<<1 | 0x2<<4},
			wantErr: false,
		},
		{
			name: "Standing still in floor 6 with obstruction",
			fields: fields{
				ID:           [4]byte{0xDE, 0xAD, 0xBE, 0xEF},
				Direction:    elevio.MD_Stop,
				Floor:        6,
				MotorRunning: true,
				StopPressed:  false,
				Obstruction:  true,
				ServingOrder: elevio.MD_Down,
			},
			want:    []byte{0xDE, 0xAD, 0xBE, 0xEF, 0, 0, 0, 6, 0x0<<6 | 0x1<<0 | 0x1<<2 | 0x2<<4},
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			e := &State{
				ID:           tt.fields.ID,
				Direction:    tt.fields.Direction,
				Floor:        tt.fields.Floor,
				MotorRunning: tt.fields.MotorRunning,
				StopPressed:  tt.fields.StopPressed,
				Obstruction:  tt.fields.Obstruction,
				ServingOrder: tt.fields.ServingOrder,
			}
			got, err := e.MarshalBinary()
			if (err != nil) != tt.wantErr {
				t.Errorf("ElevatorState.MarshalBinary() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("ElevatorState.MarshalBinary() = %x, want %x", got, tt.want)
			}
		})
	}
}

func TestElevatorState_UnmarshalBinary(t *testing.T) {
	type fields struct {
		ID           [4]byte
		Direction    elevio.MotorDirection
		Floor        int
		MotorRunning bool
		StopPressed  bool
		Obstruction  bool
		ServingOrder elevio.MotorDirection
	}
	type args struct {
		data []byte
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		wantErr bool
	}{
		{
			name: "Test going up from floor 3",
			fields: fields{
				ID:           [4]byte{0xDE, 0xAD, 0xBE, 0xEF},
				Direction:    elevio.MD_Up,
				Floor:        3,
				MotorRunning: true,
				StopPressed:  false,
				Obstruction:  false,
				ServingOrder: elevio.MD_Up,
			},
			args: args{
				data: []byte{0xDE, 0xAD, 0xBE, 0xEF, 0, 0, 0, 3, 0x1<<6 | 0x1<<0 | 0x1<<4},
			},
			wantErr: false,
		},
		{
			name: "Test going down from floor 4",
			fields: fields{
				ID:           [4]byte{0xDE, 0xAD, 0xBE, 0xEF},
				Direction:    elevio.MD_Down,
				Floor:        4,
				MotorRunning: true,
				StopPressed:  false,
				Obstruction:  false,
				ServingOrder: elevio.MD_Down,
			},
			args: args{
				data: []byte{0xDE, 0xAD, 0xBE, 0xEF, 0, 0, 0, 4, 0x2<<6 | 0x1<<0 | 0x2<<4},
			},
			wantErr: false,
		},
		{
			name: "Test standing still at floor 6",
			fields: fields{
				ID:           [4]byte{0xDE, 0xAD, 0xBE, 0xEF},
				Direction:    elevio.MD_Stop,
				Floor:        6,
				MotorRunning: true,
				StopPressed:  false,
				Obstruction:  false,
				ServingOrder: elevio.MD_Up,
			},
			args: args{
				data: []byte{0xDE, 0xAD, 0xBE, 0xEF, 0, 0, 0, 6, 0x0<<6 | 0x1<<0 | 0x1<<4},
			},
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			actual := &State{
				ID:           tt.fields.ID,
				Direction:    tt.fields.Direction,
				Floor:        tt.fields.Floor,
				MotorRunning: tt.fields.MotorRunning,
				StopPressed:  tt.fields.StopPressed,
				Obstruction:  tt.fields.Obstruction,
				ServingOrder: tt.fields.ServingOrder,
			}
			e := &State{}
			if err := e.UnmarshalBinary(tt.args.data); (err != nil) != tt.wantErr {
				t.Errorf("ElevatorState.UnmarshalBinary() error = %v, wantErr %v", err, tt.wantErr)
			}
			if !reflect.DeepEqual(actual.ID, e.ID) ||
				actual.Direction != e.Direction ||
				actual.Floor != e.Floor ||
				actual.MotorRunning != e.MotorRunning ||
				actual.StopPressed != e.StopPressed ||
				actual.Obstruction != e.Obstruction ||
				actual.ServingOrder != e.ServingOrder {
				t.Errorf("State.UnmarshalBinary() failed!\nWanted: %v, got %v\nFrom %x", actual, e, tt.args.data)
			}
		})
	}
}
