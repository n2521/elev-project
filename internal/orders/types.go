package orders

import (
	"encoding/binary"
	"errors"
	"reflect"
	"time"

	"github.com/Oskiboy/elev-project/internal/elevator"
	"github.com/TTK4145/driver-go/elevio"
)

// OrderChannels hols all channels the order manager needs
type OrderChannels struct {
	NewOrders     chan Order
	CompleteOrder chan Order

	TxNewOrder      chan Order
	TxDone          chan bool
	RxNewOrder      chan Order
	TxCompleteOrder chan Order
	RxCompleteOrder chan Order
}

// PortConfig holds all ports the manager needs
type PortConfig struct {
	NewOrderPort      int
	CompleteOrderPort int
}

const (
	orderIDLength   = 12
	orderBinarySize = orderIDLength + 2
)

// Order is a elevator cab order
type Order struct {
	ID         [orderIDLength]byte
	Floor      int
	ButtonType elevio.ButtonType
	Deadline   time.Time
}

// MarshalBinary creates a binary representation of an order.
// First orderIDLength bytes are the ID while the last two are
// button type and floor.
func (o *Order) MarshalBinary() ([]byte, error) {
	b := make([]byte, orderBinarySize)
	copy(b[0:orderIDLength], o.ID[:])
	b[orderIDLength] = uint8(o.ButtonType)
	b[orderIDLength+1] = uint8(o.Floor)
	return b, nil
}

// UnmarshalBinary populates the order object from binary data
// Bytes represent the following
// Data:	| ID 			| ButtonType	| Floor	|
// Byte#	| 0 1 2 3 4 5 6	| 7				| 8		|
func (o *Order) UnmarshalBinary(data []byte) error {
	copy(o.ID[:], data[0:orderIDLength])
	switch data[orderIDLength] {
	case 0:
		o.ButtonType = elevio.BT_HallUp
	case 1:
		o.ButtonType = elevio.BT_HallDown
	case 2:
		o.ButtonType = elevio.BT_Cab
	default:
		return errors.New("could not unmarshal button type of order")
	}
	o.Floor = int(data[orderIDLength+1])
	return nil
}

// SameID compares IDs of two orders
func (o *Order) SameID(other Order) bool {
	return reflect.DeepEqual(o.ID, other.ID)
}

// SameOrder checks if two orders can be served interchangeably.
func (o *Order) SameOrder(other Order) bool {
	return o.Floor == other.Floor && o.ButtonType == other.ButtonType
}

// Equal check if two orders are equal
func (o *Order) Equal(other Order) bool {
	return o.SameID(other) && o.SameOrder(other)
}

// AddID tags an order with an ID based on the provided elevator
// ID and _timestamp_
func (o *Order) AddID(elevatorID [4]byte) {
	copy(o.ID[0:4], elevatorID[:])
	binary.BigEndian.PutUint64(o.ID[4:], uint64(time.Now().Unix()))
}

// IsInternal returns true if the order is from ourself.
func (o *Order) IsInternal() bool {
	ID := elevator.GetState().ID
	return reflect.DeepEqual(o.ID[0:4], ID[:])
}

// TimedOut checks if the order has timed out
func (o *Order) TimedOut() bool {
	return o.Deadline.Before(time.Now())
}

// BelongsTo checks to see if an order belongs to a given elevator
// state.
func (o *Order) BelongsTo(e elevator.State) bool {
	return reflect.DeepEqual(o.ID[0:4], e.ID)
}
