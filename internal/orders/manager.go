package orders

import (
	"math"
	"os"
	"strconv"
	"sync"
	"time"

	"github.com/Oskiboy/elev-project/internal/elevator"
	"github.com/Oskiboy/elev-project/internal/peers"
	"github.com/TTK4145/driver-go/elevio"
)

const (
	// OrderTimeout tells how long a order can go without timing out
	OrderTimeout = time.Duration(15 * time.Second)
)

var (
	// We keep track of all other orders from other elevators.
	orders orderPool

	peerPool peers.PeerPool
	maxFloor = 0
)

// Run the order handler.
// The newOrders is where new orders to the elevator goes,
// while completedOrders are orders completed by the elevator
// peers is a channel where new PeerPool updates will be.
// txPort and rxPort tells where to send and receive orders
func Run(channels OrderChannels, peers chan peers.PeerPool, ports PortConfig) {
	orders = orderPool{
		orders: []Order{},
		mtx:    sync.RWMutex{},
	}

	channels.TxNewOrder = make(chan Order, 10)
	channels.TxDone = make(chan bool, 10)
	channels.RxNewOrder = make(chan Order, 100)
	channels.TxCompleteOrder = make(chan Order, 100)

	nFloor, err := strconv.Atoi(os.Getenv("NUM_FLOOR"))
	if err != nil {
		maxFloor = 4
	}
	maxFloor = nFloor

	go orderHandler(channels, peers)

	go newOrderTransmitter(ports.NewOrderPort, channels)
	go newOrderReceiver(ports.NewOrderPort, channels)

	go completedOrderTransmitter(ports.CompleteOrderPort, channels)
	go completedOrderReceiver(ports.CompleteOrderPort)
}

func orderHandler(channels OrderChannels, peersChan chan peers.PeerPool) {

	var peerPool peers.PeerPool
	buttonPresses := make(chan elevio.ButtonEvent)
	go elevio.PollButtons(buttonPresses)

	elevatorID := elevator.GetState().ID

	for {
		select {
		case btn := <-buttonPresses:
			o := Order{
				ButtonType: btn.Button,
				Floor:      btn.Floor,
				Deadline:   time.Now().Add(OrderTimeout),
			}
			o.AddID(elevatorID)
			if orders.AddOrder(o) {
				// If we are the only elevator we just take the order
				// and don't bother transmitting it.
				if peerPool.AnyPeers() {
					channels.TxNewOrder <- o
					<-channels.TxDone
					if takeOrder(o, peerPool) {
						channels.NewOrders <- o
					}
				} else {
					channels.NewOrders <- o
				}
			}

		case order := <-channels.CompleteOrder:
			channels.TxCompleteOrder <- order
			orders.ClearOrder(order)
		case newOrder := <-channels.RxNewOrder:
			newOrder.Deadline = time.Now().Add(OrderTimeout)
			if orders.AddOrder(newOrder) && takeOrder(newOrder, peerPool) {
				channels.NewOrders <- newOrder
			}
		case peerPool = <-peersChan:
		default:
			if o, hasTimeout := orders.CheckDeadlines(); hasTimeout {
				if peerPool.AnyPeers() {
					channels.TxNewOrder <- o
					<-channels.TxDone
					if takeOrder(o, peerPool) {
						channels.NewOrders <- o
					}
				} else {
					channels.NewOrders <- o
				}
			}
		}
	}
}

func takeOrder(order Order, peerPool peers.PeerPool) bool {
	state := elevator.GetState()
	if order.ButtonType == elevio.BT_Cab {
		return order.IsInternal()
	}
	selfCost := calculateCost(state, order)

	lowestCost := math.Inf(1)
	for _, p := range peerPool.Peers {
		lowestCost = math.Min(calculateCost(p.State, order), lowestCost)
	}
	// To be safe we should take the order if there is any ambiguity.
	return selfCost <= lowestCost
}

const (
	floorCost = 10.0
)

func calculateCost(s elevator.State, o Order) float64 {
	var dir elevio.ButtonType
	switch s.ServingOrder {
	case elevio.MD_Up:
		dir = elevio.BT_HallUp
	case elevio.MD_Down:
		dir = elevio.BT_HallDown
	case elevio.MD_Stop:
		dir = elevio.BT_Cab
	}

	cost := 10.0
	if o.BelongsTo(s) {
		cost -= 2.0
	}
	if !s.MotorRunning {
		cost += 10000
	}

	// Elevator is going in the same direction as the order
	if o.ButtonType == dir && (s.Floor <= o.Floor && s.ServingOrder == elevio.MD_Up ||
		s.Floor >= o.Floor && s.ServingOrder == elevio.MD_Down) {
		// If we are going in the correct direction compared to the order we only
		// penalize the distance to the order.
		cost += math.Abs(float64(s.Floor - o.Floor))

	} else if dir == elevio.BT_Cab {
		// If we are idle then we should probably be doing something
		cost += 2 * math.Abs(float64(s.Floor-o.Floor))
	} else {
		// If we are going in the complete oposite direction we should penalize that a lot.
		cost += 5 * (math.Abs(float64(s.Floor-o.Floor)) + math.Abs(float64(s.Floor-maxFloor)))
	}

	return cost
}
