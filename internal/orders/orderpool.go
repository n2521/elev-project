package orders

import (
	"log"
	"sync"

	"github.com/TTK4145/driver-go/elevio"
)

type orderPool struct {
	orders            []Order
	mtx               sync.RWMutex
	recentlyAdded     []Order
	recentlyCompleted []Order
}

// AddOrder adds the given order to the order managers order list
func (op *orderPool) AddOrder(order Order) bool {
	op.mtx.RLock()
	for _, o := range op.orders {
		if o.Equal(order) {
			op.mtx.RUnlock()
			return false
		}
	}
	op.mtx.RUnlock()
	op.mtx.Lock()
	op.orders = append(op.orders, order)
	op.mtx.Unlock()
	if order.ButtonType != elevio.BT_Cab || order.IsInternal() {
		elevio.SetButtonLamp(order.ButtonType, order.Floor, true)
	}
	return true
}

// ClearOrder clears the order from the pool if possible
func (op *orderPool) ClearOrder(order Order) {
	elevio.SetButtonLamp(order.ButtonType, order.Floor, false)
	op.mtx.Lock()
	defer op.mtx.Unlock()
	nOrders := len(op.orders)
	if nOrders == 0 {
		return
	}
	if nOrders == 1 {
		if op.orders[0].SameOrder(order) {
			op.orders = []Order{}
		} else {
			return
		}
	} else {
		for i := range op.orders {
			if op.orders[i].SameOrder(order) {
				copy(op.orders[i:], op.orders[i+1:])
				break
			}
		}
		op.orders = op.orders[:len(op.orders)-1]
	}
	if nOrders == len(op.orders) {
		log.Println("ERROR: Didn't clear any orders!")
	}
}

// CheckDeadlines checks if there are any orders which deadlines
// are expired.
func (op *orderPool) CheckDeadlines() (Order, bool) {
	op.mtx.RLock()
	defer op.mtx.RUnlock()
	for _, o := range op.orders {
		if o.TimedOut() {
			return o, true
		}
	}
	return Order{}, false
}

// InRecentlyAdded checks if the given order is in the
// recently added buffer. This is used to check if a order
// was added to the pool recently and to reduce the workload of the
// actual orderpool.
func (op *orderPool) InRecentlyAdded(o Order) bool {
	for _, order := range op.recentlyAdded {
		if order.SameID(o) {
			return true
		}
	}
	return false
}

// InRecentlyCompleted checks if the given order is completed recently.
// This is also to reduce the actual access of the main orderpool as we
// receive a lot of copies of each of the orders that needs completion.
func (op *orderPool) InRecentlyCompleted(o Order) bool {
	for _, order := range op.recentlyCompleted {
		if order.SameID(o) {
			return true
		}
	}
	return false
}

// ClearNonerecentlyCompleted will check if some orders in the recentlyCompleted
// cache are old and then remove them.
func (op *orderPool) ClearNoneRecentlyCompleted() {
	nOrders := len(op.recentlyCompleted)
	if nOrders == 0 {
		return
	}
	if nOrders == 1 {
		if op.recentlyCompleted[0].TimedOut() {
			op.recentlyCompleted = []Order{}
		}
	} else {
		for i := range op.recentlyCompleted {
			if op.recentlyCompleted[i].TimedOut() {
				copy(op.recentlyCompleted[i:], op.recentlyCompleted[i+1:])
				nOrders--
			}
		}
		if nOrders != 0 {
			op.recentlyCompleted = op.recentlyCompleted[:nOrders-1]
		} else {
			op.recentlyCompleted = []Order{}
		}
	}
}

// ClearNoneRecentlyAdded will check if some orders in the recentlyAdded
// cache are old and then remove them.
func (op *orderPool) ClearNoneRecentlyAdded() {
	nOrders := len(op.recentlyAdded)
	if nOrders == 0 {
		return
	}
	if nOrders == 1 {
		if op.recentlyAdded[0].TimedOut() {
			op.recentlyAdded = []Order{}
		}
	} else {
		for i := range op.recentlyAdded {
			if op.recentlyAdded[i].TimedOut() {
				copy(op.recentlyAdded[i:], op.recentlyAdded[i+1:])
				nOrders--
			}
		}
		if nOrders != 0 {
			op.recentlyAdded = op.recentlyAdded[:nOrders-1]
		} else {
			op.recentlyAdded = []Order{}
		}
	}
}
