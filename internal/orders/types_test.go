package orders

import (
	"reflect"
	"testing"
	"time"

	"github.com/TTK4145/driver-go/elevio"
)

func TestOrder_MarshalBinary(t *testing.T) {
	type fields struct {
		ID         [orderIDLength]byte
		Floor      int
		ButtonType elevio.ButtonType
		Deadline   time.Time
	}
	tests := []struct {
		name    string
		fields  fields
		want    []byte
		wantErr bool
	}{
		{
			name: "Order going up from floor 1 with ID 0xA5A5A5",
			fields: fields{
				ID:         [orderIDLength]byte{0xA, 0x5, 0xA, 0x5, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0},
				Floor:      1,
				ButtonType: elevio.BT_HallUp,
			},
			want:    []byte{0xA, 0x5, 0xA, 0x5, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x1},
			wantErr: false,
		},
		{
			name: "Order going down from floor 1 with ID 0xA5A5A5",
			fields: fields{
				ID:         [orderIDLength]byte{0xA, 0x5, 0xA, 0x5, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0},
				Floor:      1,
				ButtonType: elevio.BT_HallDown,
			},
			want:    []byte{0xA, 0x5, 0xA, 0x5, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x1, 0x1},
			wantErr: false,
		},
		{
			name: "Cab order from floor 1 with ID 0xA5A5A5",
			fields: fields{
				ID:         [orderIDLength]byte{0xA, 0x5, 0xA, 0x5, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0},
				Floor:      1,
				ButtonType: elevio.BT_Cab,
			},
			want:    []byte{0xA, 0x5, 0xA, 0x5, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x2, 0x1},
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			o := &Order{
				ID:         tt.fields.ID,
				Floor:      tt.fields.Floor,
				ButtonType: tt.fields.ButtonType,
				Deadline:   tt.fields.Deadline,
			}
			got, err := o.MarshalBinary()
			if (err != nil) != tt.wantErr {
				t.Errorf("Order.MarshalBinary() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("Order.MarshalBinary() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestOrder_UnmarshalBinary(t *testing.T) {
	type args struct {
		data []byte
	}
	tests := []struct {
		name    string
		result  *Order
		args    args
		wantErr bool
	}{
		{
			name: "Order going up from floor 1 with ID 0xA5A5A5",
			result: &Order{
				ID:         [orderIDLength]byte{0xA, 0x5, 0xA, 0x5, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0},
				Floor:      1,
				ButtonType: elevio.BT_HallUp,
			},
			args: args{
				data: []byte{0xA, 0x5, 0xA, 0x5, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x1},
			},
			wantErr: false,
		},
		{
			name: "Order going down from floor 1 with ID 0xA5A5A5",
			result: &Order{
				ID:         [orderIDLength]byte{0xA, 0x5, 0xA, 0x5, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0},
				Floor:      1,
				ButtonType: elevio.BT_HallDown,
			},
			args: args{
				data: []byte{0xA, 0x5, 0xA, 0x5, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x1, 0x1},
			},
			wantErr: false,
		},
		{
			name: "Cab order from floor 1 with ID 0xA5A5A5",
			result: &Order{
				ID:         [orderIDLength]byte{0xA, 0x5, 0xA, 0x5, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0},
				Floor:      1,
				ButtonType: elevio.BT_Cab,
			},
			args: args{
				data: []byte{0xA, 0x5, 0xA, 0x5, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x2, 0x1},
			},
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			o := &Order{}
			if err := o.UnmarshalBinary(tt.args.data); (err != nil) != tt.wantErr {
				t.Errorf("Order.UnmarshalBinary() error = %v, wantErr %v", err, tt.wantErr)
			}
			if o.Floor != tt.result.Floor ||
				!reflect.DeepEqual(o.ID, tt.result.ID) ||
				o.ButtonType != tt.result.ButtonType {
				t.Errorf("Order.UnmarshalBinary() failed, wanted %v, got %v", tt.result, o)
			}
		})
	}
}

func TestOrder_AddID(t *testing.T) {
	type fields struct {
		ID         [orderIDLength]byte
		Floor      int
		ButtonType elevio.ButtonType
		Deadline   time.Time
	}
	type args struct {
		elevatorID [4]byte
	}
	tests := []struct {
		name   string
		fields fields
		args   args
	}{
		{
			name:   "Add an ID",
			fields: fields{},
			args: args{
				elevatorID: [4]byte{0xDE, 0xAD, 0xBE, 0xEF},
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			o := &Order{
				ID:         tt.fields.ID,
				Floor:      tt.fields.Floor,
				ButtonType: tt.fields.ButtonType,
				Deadline:   tt.fields.Deadline,
			}
			o.AddID(tt.args.elevatorID)
		})
	}
}
