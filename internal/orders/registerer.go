package orders

import (
	"fmt"
	"log"
	"net"
	"time"

	"github.com/Oskiboy/elev-project/internal/network/conn"
)

const (
	// OrderDeadline sets how long before an orders
	// watchdog triggers
	OrderDeadline = 30 * time.Second
	// writeTimeout is how long we permit the write operation to take before we
	// assume that we are disconnected.
	writeTimeout = 2 * time.Second
	// nPackages decides how many packages we send each time we send a package.
	nPackages = 40
	// nReaders decides how many reader routines we run, this is to efficiently read
	// the high amount of received packages
	nReaders = 10
	// readChannelSize decides how many orders we should be able to stuff
	// in one channel
	readChannelSize = 1000
)

// newOrderTransmitter takes care of the broadcasting of new orders to peers.
// When an order is placed in the txOrder channel it will be broadcast and the
// result of the attempted broadcast will be put in the txDone channel.
func newOrderTransmitter(port int, channels OrderChannels) {

	addr, err := net.ResolveUDPAddr("udp", fmt.Sprintf("255.255.255.255:%d", port))
	if err != nil {
		log.Panicln("newOrderTransmitter could not resolve UDP address, error:", err)
	}

	nConn, err := net.DialUDP("udp", nil, addr)
	if err != nil {
		log.Panicln("newOrderTransmitter could not establist UDP connection, error:", err)
	}
	defer nConn.Close()

	log.Println("Running new order transmitter")
	for {
		select {
		case order := <-channels.TxNewOrder:
			go transmitOrder(order, nConn, channels.TxDone)
		}
	}
}

// newOrderReceiver reads all broadcasted new received orders.
// rxOrder should be able to hold at least a couple of hundred orders.
// port of the receiver should match up with the port of the transmitter
// of other elevators.
func newOrderReceiver(port int, channels OrderChannels) {

	nConn, err := conn.DialBroadcastUDP(port)
	if err != nil {
		log.Panicln("newOrderReceiver could not establish connection to UDP broadcast, error:", err)
	}
	defer nConn.Close()

	newOrders := make(chan Order, readChannelSize)
	for i := 0; i < nReaders; i++ {
		go readerRoutine(nConn, newOrders)
	}

	clearDeadline := time.After(2 * time.Second)

	log.Println("Running new order receiver")
	for {
		select {
		case <-clearDeadline:
			orders.ClearNoneRecentlyAdded()
			clearDeadline = time.After(2 * time.Second)
		case o := <-newOrders:
			if !orders.InRecentlyAdded(o) {
				orders.recentlyAdded = append(orders.recentlyAdded, o)
				channels.RxNewOrder <- o
			}
		}
	}
}

// completedOrderTransimtter will transmit completed orders to peers.
// Orders put in the completedOrders channel will be transmitted to peers
// and upon being read will be considered completed.
func completedOrderTransmitter(port int, channels OrderChannels) {

	cAddr, err := net.ResolveUDPAddr("udp", fmt.Sprintf("255.255.255.255:%d", port))
	if err != nil {
		log.Panicln("completedOrderTransmitter could not resolve UDP address, error:", err)
	}

	conn, err := net.DialUDP("udp", nil, cAddr)
	if err != nil {
		log.Panicln("completedOrderTransmitter could not establish a UDP connection, error:", err)
	}
	defer conn.Close()

	log.Println("Running completed order transmitter")
	for {
		select {
		case order := <-channels.TxCompleteOrder:
			go transmitOrder(order, conn, nil)
		}
	}
}

// completedOrderReceiver will read all broadcasted completed orders on the
// port provided. It will also cache which orders have been completed
// recently to not overflow the order manager. The completeOrder channel
// should be able to hold at least a dousin orders.
func completedOrderReceiver(port int) {

	nConn, err := conn.DialBroadcastUDP(port)
	if err != nil {
		log.Panicln("completedOrderReceiver could not establish connection to UDP broadcast, error:", err)
	}
	defer nConn.Close()

	completed := make(chan Order, readChannelSize)
	for i := 0; i < nReaders; i++ {
		go readerRoutine(nConn, completed)
	}
	clearDeadline := time.After(2 * time.Second)
	log.Println("Running completed order receiver")
	for {
		select {
		case <-clearDeadline:
			orders.ClearNoneRecentlyCompleted()
			clearDeadline = time.After(2 * time.Second)
		case o := <-completed:

			// It should be cached for a few seconds
			o.Deadline = time.Now().Add(2 * time.Second)
			if !orders.InRecentlyCompleted(o) {
				orders.recentlyCompleted = append(orders.recentlyCompleted, o)
				orders.ClearOrder(o)
			}
		}
	}
}

func readerRoutine(nConn net.PacketConn, result chan Order) {
	buf := make([]byte, orderBinarySize+10)
	for {
		_, _, err := nConn.ReadFrom(buf[:])
		if err != nil {
			log.Println("[ERROR] Could not read from UDP connection")
			continue
		}
		o := Order{}
		err = o.UnmarshalBinary(buf[:])
		if err != nil {
			log.Println("[ERROR] Could not unmarshal binary!\nGot:", buf[:])
			continue
		}
		result <- o
	}
}

func transmitOrder(order Order, nConn *net.UDPConn, TxDone chan bool) {
	nConn.SetWriteDeadline(time.Now().Add(writeTimeout))

	bin, err := order.MarshalBinary()
	if err != nil {
		log.Println("Could not marshal order binary! Error:", err)
		if TxDone != nil {
			TxDone <- false
		}
		return
	}
	for i := 0; i < nPackages; i++ {
		_, err := nConn.Write(bin)
		if err != nil {
			log.Println("[ERROR] Could not write to broadcast!")
		}
	}
	if TxDone != nil {
		TxDone <- true
	}
}
