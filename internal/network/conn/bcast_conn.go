// +build !windows

package conn

import (
	"net"
	"os"
	"syscall"
)

// DialBroadcastUDP creates a new UDP broadcast socket.
func DialBroadcastUDP(port int) (net.PacketConn, error) {
	s, err := syscall.Socket(syscall.AF_INET, syscall.SOCK_DGRAM, syscall.IPPROTO_UDP)
	if err != nil {
		return nil, err
	}
	err = syscall.SetsockoptInt(s, syscall.SOL_SOCKET, syscall.SO_REUSEADDR, 1)
	if err != nil {
		return nil, err
	}
	err = syscall.SetsockoptInt(s, syscall.SOL_SOCKET, syscall.SO_BROADCAST, 1)
	if err != nil {
		return nil, err
	}
	err = syscall.Bind(s, &syscall.SockaddrInet4{Port: port})
	if err != nil {
		return nil, err
	}

	f := os.NewFile(uintptr(s), "")
	conn, err := net.FilePacketConn(f)
	f.Close()
	if err != nil {
		return nil, err
	}

	return conn, nil
}
