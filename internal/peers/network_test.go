package peers

import (
	"reflect"
	"testing"

	"github.com/TTK4145/driver-go/elevio"

	"github.com/Oskiboy/elev-project/internal/elevator"
)

func TestPeer_UnmarshalBinary(t *testing.T) {
	type fields struct {
		ID    [4]byte
		State elevator.State
	}
	type args struct {
		data []byte
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		wantErr bool
	}{
		{
			name: "DEADBEEF ID going up and in floor 1",
			fields: fields{
				ID: [4]byte{0xDE, 0xAD, 0xBE, 0xEF},
				State: elevator.State{
					MotorRunning: true,
					Floor:        1,
					Direction:    1,
					ID:           [4]byte{0xDE, 0xAD, 0xBE, 0xEF},
				},
			},
			args:    args{data: []byte{0xDE, 0xAD, 0xBE, 0xEF, 0, 0, 0, 1, 0x1<<6 | 0x1<<0}},
			wantErr: false,
		},
		{
			name: "DEADBEEF ID going down and in floor 1",
			fields: fields{
				ID: [4]byte{0xDE, 0xAD, 0xBE, 0xEF},
				State: elevator.State{
					MotorRunning: true,
					Floor:        1,
					Direction:    -1,
					ID:           [4]byte{0xDE, 0xAD, 0xBE, 0xEF},
				},
			},
			args:    args{data: []byte{0xDE, 0xAD, 0xBE, 0xEF, 0, 0, 0, 1, 0x2<<6 | 0x1<<0}},
			wantErr: false,
		},
		{
			name: "Zero ID standing still in floor 1",
			fields: fields{
				ID: [4]byte{0, 0, 0, 0},
				State: elevator.State{
					MotorRunning: true,
					Floor:        1,
					Direction:    0,
					ID:           [4]byte{0, 0, 0, 0},
				},
			},
			args:    args{data: []byte{0, 0, 0, 0, 0, 0, 0, 1, 0x0<<6 | 0x1<<0}},
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			p := &Peer{}
			if err := p.UnmarshalBinary(tt.args.data); (err != nil) != tt.wantErr {
				t.Errorf("Peer.UnmarshalBinary() error = %v, wantErr %v", err, tt.wantErr)
			}
			if p.ID != tt.fields.ID {
				t.Errorf("Not equal")
			}
			if p.State != tt.fields.State {
				t.Errorf("State was not unmarshalled correctly!\nWant %v, got %v", tt.fields.State, p.State)
			}
		})
	}
}

func TestPeer_MarshalBinary(t *testing.T) {
	type fields struct {
		ID    [4]byte
		State elevator.State
	}
	tests := []struct {
		name    string
		fields  fields
		want    []byte
		wantErr bool
	}{
		{
			name: "Zero ID going up and in floor 1",
			fields: fields{
				ID: [4]byte{0, 0, 0, 0},
				State: elevator.State{
					MotorRunning: true,
					Floor:        1,
					Direction:    1,
					ID:           [4]byte{0, 0, 0, 0},
				},
			},
			want:    []byte{0, 0, 0, 0, 0, 0, 0, 1, 0x1<<6 | 0x1<<0x0},
			wantErr: false,
		},
		{
			name: "Zero ID going down and in floor 1",
			fields: fields{
				ID: [4]byte{0, 0, 0, 0},
				State: elevator.State{
					MotorRunning: true,
					Floor:        1,
					Direction:    elevio.MD_Down,
					ID:           [4]byte{0, 0, 0, 0},
				},
			},
			want:    []byte{0, 0, 0, 0, 0, 0, 0, 1, 0x2<<6 | 0x1<<0x0},
			wantErr: false,
		},
		{
			name: "Zero ID standing still and in floor 1",
			fields: fields{
				ID: [4]byte{0, 0, 0, 0},
				State: elevator.State{
					MotorRunning: true,
					Floor:        1,
					Direction:    0,
					ID:           [4]byte{0, 0, 0, 0},
				},
			},
			want:    []byte{0, 0, 0, 0, 0, 0, 0, 1, 0x0<<6 | 0x1<<0x0},
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			p := &Peer{
				ID:    tt.fields.ID,
				State: tt.fields.State,
			}
			got, err := p.MarshalBinary()
			if (err != nil) != tt.wantErr {
				t.Errorf("Peer.MarshalBinary() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("Peer.MarshalBinary() = %v, want %v", got, tt.want)
			}
		})
	}
}
