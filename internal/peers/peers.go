package peers

import (
	"encoding/binary"
	"fmt"
	"log"
	"net"
	"time"

	"github.com/Oskiboy/elev-project/internal/elevator"
	"github.com/Oskiboy/elev-project/internal/network/conn"
)

const (
	broadcastInterval = 10 * time.Millisecond        // How long before we time out a UDP read
	timeout           = 100 * time.Millisecond       // How long before a peer is lost
	statePacketSize   = elevator.StateBinarySize + 4 // One state packet is 11 bytes
)

var (
	pool PeerPool
)

// Run the peer transmitter and receiver
func Run(port int, peerPool chan PeerPool) {
	log.Println("Starting peer transmitter")
	go transmitter(port)
	log.Println("Starting peer receiver")
	go receiver(port, peerPool)
}

// Transmitter starts a transmitter that updates the
// local state to peers.
func transmitter(port int) {
	log.Println("Connecting to UDP broadcast")
	conn, err := conn.DialBroadcastUDP(port)
	if err != nil {
		log.Panicln("Peer transmitter could not establish connection with broadcast, error:", err)
	}
	defer conn.Close()

	addr, err := net.ResolveUDPAddr("udp4", fmt.Sprintf("255.255.255.255:%d", port))
	if err != nil {
		log.Fatalln(err.Error())
	}

	state := elevator.GetState()

	log.Println("Running broadcast loop")
	for {
		select {
		case <-time.After(broadcastInterval):
			state = elevator.GetState()
			statePacket, err := state.MarshalBinary()
			if err != nil {
				log.Println("[ERROR] Could not marshal the StatePacket:", state)
				continue
			}
			_, err = conn.WriteTo(statePacket, addr)
			if err != nil {
				log.Println("[ERROR] Could not write to the peer broadcast connection!")
			}
		}
	}
}

// Receiver starts a receiver that reads the state of
// other peers on the network.
func receiver(port int, peerUpdateCh chan<- PeerPool) {
	var buf [statePacketSize]byte
	var p PeerPool
	lastSeen := make(map[uint32]Peer)
	rawID := elevator.GetState().ID
	selfID := binary.BigEndian.Uint32(rawID[:])
	conn, err := conn.DialBroadcastUDP(port)
	if err != nil {
		log.Panicln("Peer receiver could not stablish connection with UDP broadcast, error:", err)
	}
	defer conn.Close()

	for {
		conn.SetReadDeadline(time.Now().Add(broadcastInterval))
		_, _, err := conn.ReadFrom(buf[:])
		if err != nil {
			continue
		}
		var peer Peer
		//fmt.Println("Read:  ", buf)
		err = peer.UnmarshalBinary(buf[:])
		if err != nil {
			log.Printf("Could not unmarshal peer from data: %x\nError: %s\n", buf[:11], err.Error())
		}
		peer.timeOut = time.Now().Add(timeout)

		id := peer.UUID()
		if id == selfID {
			continue
		}

		// Adding new connection
		p.New = false
		if id != 0 {
			_, idExists := lastSeen[id]
			if !idExists {
				p.New = true
			}
			lastSeen[id] = peer
		}

		// Removing dead connection
		for id, v := range lastSeen {
			if v.TimedOut() {
				delete(lastSeen, id)
			}
		}

		// Sending update
		p.Peers = make([]Peer, 0, len(lastSeen))

		for _, v := range lastSeen {
			p.Peers = append(p.Peers, v)
		}

		peerUpdateCh <- p
	}
}
