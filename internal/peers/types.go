package peers

import (
	"encoding/binary"
	"time"

	"github.com/Oskiboy/elev-project/internal/elevator"
)

// Peer holds the data sent to and from peers
type Peer struct {
	ID      [4]byte
	timeOut time.Time
	State   elevator.State
}

// UUID returns the ID of a peer as a uint32
func (p *Peer) UUID() uint32 {
	return binary.BigEndian.Uint32(p.ID[:])
}

// TimedOut checks wether or not the peer has timed out.
func (p *Peer) TimedOut() bool {
	return p.timeOut.Before(time.Now())
}

// MarshalBinary turns Peer into bytes
func (p *Peer) MarshalBinary() ([]byte, error) {
	state, err := p.State.MarshalBinary()
	if err != nil {
		return []byte{}, err
	}
	return state, nil
}

// UnmarshalBinary extracts a Peer from binary data.
func (p *Peer) UnmarshalBinary(data []byte) error {
	err := p.State.UnmarshalBinary(data[:])
	if err != nil {
		return err
	}
	copy(p.ID[:], data[:4])
	return nil
}

// PeerPool is an abstraction of which
// peers are added and lost from network.
type PeerPool struct {
	Peers []Peer
	New   bool
}

// AnyPeers checks if there are any peers in the peer pool
func (pp *PeerPool) AnyPeers() bool {
	return len(pp.Peers) != 0
}
