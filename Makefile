COMPOSE_PATH=docker/docker-compose.yml
ELEVATOR_ID=1111
ELEVATOR_ADDRESS=localhost:9000
NUM_FLOOR=4

.PHONY: build run all docker-build docker-run
all: init
	go build -o build/elevator cmd/elevator/main.go

init: go.mod
	go mod download

run: all
	ELEVATOR_ID=${ELEVATOR_ID}\
	ELEVATOR_ADDRESS=${ELEVATOR_ADDRESS}\
	NUM_FLOOR=${NUM_FLOOR}\
	. build/elevator

docker-build:
	docker-compose -f ${COMPOSE_PATH} build

docker-run: docker-build
	docker-compose -f ${COMPOSE_PATH} up
