package main

import (
	"fmt"
	"log"
	"os"
	"os/exec"
	"os/signal"
	"runtime"
	"syscall"
	"time"

	"github.com/Oskiboy/elev-project/internal/controller"
	"github.com/Oskiboy/elev-project/internal/elevator"
	"github.com/Oskiboy/elev-project/internal/orders"
	"github.com/Oskiboy/elev-project/internal/peers"
)

const (
	peerPort           = 9100
	newOrderPort       = 9101
	completedOrderPort = 9102
)

var globalState peers.PeerPool

func main() {
	fmt.Println("\033[2J")
	runtime.GOMAXPROCS(runtime.NumCPU())
	cmd := exec.Command("sleep", "100")
	cmd.Start()
	var ID = [4]byte{0, 0, 0, 1}
	if envID := os.Getenv("ELEVATOR_ID"); envID != "" {
		copy(ID[:], envID)
	}
	log.Println("ID:", ID)

	log.Println("Initializing elevator")
	elevator.Initialize(ID)

	peerChan := make(chan peers.PeerPool, 3)
	peerPrintChan := make(chan peers.PeerPool, 3)

	orderPorts := orders.PortConfig{
		NewOrderPort:      newOrderPort,
		CompleteOrderPort: completedOrderPort,
	}

	channels := orders.OrderChannels{
		NewOrders:       make(chan orders.Order, 10),
		CompleteOrder:   make(chan orders.Order, 10),
		RxCompleteOrder: make(chan orders.Order, 100),
	}

	go controller.Run(channels.NewOrders, channels.RxCompleteOrder, channels.CompleteOrder)
	go orders.Run(channels, peerChan, orderPorts)
	go peers.Run(peerPort, peerPrintChan)

	var printTimeout time.Time = time.Now()
	sigs := make(chan os.Signal, 1)
	signal.Notify(sigs, syscall.SIGINT, syscall.SIGTERM)
	running := true
	for running {
		select {
		case p := <-peerPrintChan:
			peerChan <- p
			if time.Now().After(printTimeout) {
				fmt.Println(p.Peers[0].ID)
				printTimeout = time.Now().Add(2 * time.Second)
			}
		case <-sigs:
			running = false
		}

	}

}
