#!/usr/bin/env bash

elev_reenable_ports() {
    sudo iptables -F
}

elev_disconnect_elevators() {
    sudo iptables -F
    for port in {9100..9104}; do
        sudo iptables -A INPUT -p udp --dport $port -m statistic --mode random --probability 1 -j DROP
    done
}

elev_enable_packet_drop() {
    sudo iptables -F
    for port in {9100..9104}; do
        sudo iptables -A INPUT -p udp --dport $port -m statistic --mode random --probability 0.2 -j DROP
    done
}
