#!/bin/sh

tmux new-session -d -s foo 'SimElevatorServer --port 9000'
tmux split-window -h 'SimElevatorServer --port 9001'
tmux -2 attach-session -t foo