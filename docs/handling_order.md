@startuml
title Normal operation\n(Complete one order)
actor User
participant Buttons
participant Motor
participant ElevatorController
participant Network
participant UDP
participant Peers

User -> Buttons: Call cab button pressed
activate User
loop continuously
    Network <--> UDP: Update state
    UDP --> Peers : Read state
end
note right
    The network module always 
    updates the peers with the
    internal state and reads
    the peers state.
end note
Buttons -> ElevatorController : A button event is registered
ElevatorController -> Network: A new order is\ncreated and needs\nto be synced
Activate Network

Network --> UDP: The new order\nis sent to peers
UDP --> Peers : Read new order
Network -> Network : Order packets sent
Network -> ElevatorController : No error
deactivate Network


ElevatorController -> ElevatorController : Calculate cost and\ndecide to handle order
activate ElevatorController
ElevatorController -> Motor : Send cab 
Motor -> User: Cab arrived
deactivate User
Motor -> ElevatorController : Floor reached
ElevatorController -> Network : Complete order
Network --> UDP : Notify Peers
Peers <-- UDP: Register order\ncompletion


Network -> ElevatorController : Order completion\nregistered
deactivate ElevatorController
ElevatorController -> ElevatorController : Delete order\nfrom queue
@enduml
