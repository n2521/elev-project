@startuml
title Module overview

class Main {
    -state ElevatorState
    -id int

    +RunElevator()
}
note right: This is our entrypoint

class Network {
    --State sync --
    +TXPublishState <-chan ElevatorState
    +RxGlobalState chan<- []ElevatorState
    -- Order interface --
    +RxOrders chan<- Order
    +TxOrder <-chan Order
    +TxCompletedOrder <-chan Order
    --
    -peers Peers
    -updatePeers() error
    +RunServer()
}
class OrderManager {
    +Run(newOrders chan<- Order, completedOrders <-chan Order, txPort, rxPort int, peerPool <-chan PeerPool)
}
package ElevatorControl {
class ElevatorController {
    +Run(newOrders <-chan Order, completedOrders chan<- Order)
    -calculateCost([]ElevatorState, Order) (int, error)
}
note left: Handles all control of the elevator\n and fulfilment of orders

class Elevator {
    -state State
    +GetState() State
    +SetMotorDirection(dir elevio.MotorDirection)
}
}

@enduml