@startuml
Button -> ButtonHandler
ButtonHandler -> Transmitter
Transmitter -> LocalOrders
Transmitter -> UDP
LocalOrders -> ElevatorController
ElevatorController -> Elevator


@enduml