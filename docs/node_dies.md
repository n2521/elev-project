@startuml
title A node that handles and order dies\n(System level)

actor User
participant Peer
participant UDP
participant OtherPeers

User -> Peer : Calls cab
Peer <-> UDP : Register new order
OtherPeers <- UDP : Reads order
activate OtherPeers
Peer -> Peer : Handles order
Peer x-x UDP : Network is lost
...Time passes...
OtherPeers -> OtherPeers : Order times out
destroy OtherPeers

OtherPeers -> OtherPeers : Some other peer\nhandles the order.\nDisconnected Peer is\nno longer considered\nin cost function



@enduml