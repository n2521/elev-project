@startuml
title Loss of network during order handling
actor User
participant Buttons
participant Motor
participant ElevatorController
participant Network
participant UDP

User -> Buttons: Call cab button pressed 
Network <--> UDP: Update peers
Buttons -> ElevatorController : Register new order
Network <--> UDP: Update peers
ElevatorController -> Network: Register new order
activate Network
Network -->x UDP: Register new order
Network -->x UDP: Updates peers
Network --> Network : Connection is lost
Network -> ElevatorController : Network is lost
deactivate Network
ElevatorController -> ElevatorController : Go to single elevator mode
ElevatorController -> Motor : Run motor to order
activate Motor
...Elevator handles all new orders internally...


@enduml