@startuml
title Data models
class ElevatorState {
    +Direction int
    +Floor int
    +MotorDead bool
    -motorWatchdog timer

    +MarshalBinary() []byte
    +UnmarshalBinary(data []byte) ElevatorState
}

class Order {
    +Floor int
    +Direction int
    +CabOrder bool
    +Deadline time
    +ID string
}

class OrderPacket {
    +ID string
    +MarshalBinary() ([]data, error)
    +UnmarshalBinary([]data) error
}

class Peers {
    +Addr string
    +ID string
}

class MotorCommand {
    +Direction int
}

class LightCommand {
    +Floor int
    +InCab bool
    +On bool
}

class DoorCommand {
    +bool
}

OrderPacket o- Order
@enduml