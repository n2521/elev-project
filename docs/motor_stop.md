@startuml
title Motor stops while handling an order.
actor User
participant Buttons
participant Motor
participant ElevatorController
Participant Network
Participant UDP
participant Peers

User -> Buttons: Orders cab
activate User
Buttons -> ElevatorController: Put order in order channel
ElevatorController -> Network: Put new order in\nregister order channel
Network <--> UDP : Notify peers
UDP --> Peers : Read new order
activate Peers
Network -> ElevatorController : Order is registered
ElevatorController -> ElevatorController : Check cost of handling\norder and assign it to self.
ElevatorController -> Motor : Start motor in correct direction
activate Motor
Motor -> Motor : Run motor
Motor -> Motor : Register that motor is stopped
destroy Motor
Motor -> ElevatorController : Update motor state
ElevatorController -> Network : Update peers with state
Network <--> UDP : Notify peers
UDP --> Peers : Read updated state
...Time passes...
Peers -> Peers : Order deadline\nruns out
destroy Peers

Peers -> Peers : Calculate cost of\norder and handle it
activate Peers
Peers -> User : Send cab
deactivate User
Peers <--> UDP : Signal order completion
deactivate Peers
@enduml